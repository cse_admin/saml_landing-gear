template HySetTemplate
			
	//interface to the computing module
	formula RetGearVOrder;
	formula ExtGearVOrder;
	formula CloseDoorVOrder;
	formula OpenDoorVOrder; 
	
	//----------------------------------
	
	formula CircuitPres;
	
	constant int gearunlockup;
	constant int gearlockup;
	constant int gearunlockdown;
	constant int gearlockdown;
	constant int geardown;
	constant int gearup;
	
	constant int doorunlockup;
	constant int doorlockup;
	constant int doorlockdown;
	constant int doordown;
	constant int doorup;
	
	//enum cylinder := [down , high , intermediate];
	
	// The door always follows the cylinder states -> if the corresponding cylinder is in down position the door is open, 
	// if the cylinder is in intermediate position the door is in intermediate state and if the cylinder is in high position 
	// the door is closed
	formula DoorOpen := CylinderDoor.DoorCylinderDown;// CylinderDoor.cylinderState = cylinder.down;
	//formula DoorIntermediate := CylinderDoor.DoorCylinderInter; //CylinderDoor.cylinderState = cylinder.intermediate;
	formula DoorClosed := CylinderDoor.DoorCylinderHigh; //CylinderDoor.cylinderState = cylinder.high;
	
	// The gear always follows the cylinder states (physically)
	formula GearExt := CylinderGear.GearCylinderDown;//CylinderGear.cylinderState = cylinder.down;
	//formula GearIntermediate := CylinderGear.GearCylinderInter;//CylinderGear.cylinderState = cylinder.intermediate;
	formula GearRet := CylinderGear.GearCylinderHigh; //CylinderGear.cylinderState = cylinder.high;
		
	// Shockabsorber tells wether the plain is on the ground or flying	
	formula Ground := GearExt;
			
	//The cylinders move if there is hydraulic pressure in the opposite direction of their current position
	component CylinderGear
		
		// The two components GearExtending and GearRetracting are the timer, which count the time for the whole sequence, from one locked position to the other, as long as the required valve is open.
		// When no valve is open the components stay idle.
		// When the opposite component unlocks the gear the state is set to -1, so the locked down and unlocked formulas get reseted.
		// Example: In the initial state the gear is extended, there for the state for GearExtending is at its maximum movingdown and GearRetracting is at 0.
		// As long as both ValveOrders are not triggered this stays that way.
		// If the ExtGearVOrder is triggered, nothing happens, since the cylinder is already extended.
		// If the RetGearVOrder is triggered, than the GearRetracting component will start counting up.
		// Once it unlocks the extended gear the formula for GearUnlockedDown gets true and GearExtending is set to moveddown. This ensures, that if the valve order changes the sequence is not starting at 0.
		// As the time passes GearRetracting will reach the point, where gearup gets true. At this state GearExtending is set to gearunlockup.
		// Finally the gear will be set to lockedup and GearExtending is set to 0.
		// Now the sequence reached the exact opponent of the initial state.
		 
		component GearExtending
			constant int moveddown:=gearunlockup+geardown;
			constant int movingdown:=gearunlockup+geardown+gearlockdown;
			movedown: [0 .. movingdown] init movingdown;
			
			!ExtGearVOrder & !GearUnlockedDown & !GearUp & !GearLockedUp -> choice: ((movedown'=movingdown));
			!ExtGearVOrder & GearUnlockedDown & !GearUp & !GearLockedUp -> choice: ((movedown'=moveddown));
			!ExtGearVOrder & GearUnlockedDown & GearUp & !GearLockedUp -> choice: ((movedown'=gearunlockup));
			!ExtGearVOrder & GearUnlockedDown & GearUp & GearLockedUp -> choice: ((movedown'=0));
			
			ExtGearVOrder & !(movedown = movingdown) -> choice: ((movedown' = movedown+1));
			ExtGearVOrder & movedown = movingdown -> choice: ((movedown'=movedown));
			
			formula GearUnlockedUp := movedown >= gearunlockup;
			formula GearDown := movedown >= moveddown;
			formula GearLockedDown := movedown = movingdown;
		endcomponent
		
		component GearRetracting
			constant int movedup:=gearunlockdown+gearup;
			constant int movingup:=gearunlockdown+gearup+gearlockup;
			moveup: [0 .. movingup] init 0;
			
			!RetGearVOrder & !GearUnlockedUp & !GearDown & !GearLockedDown -> choice: ((moveup'=movingup));
			!RetGearVOrder & GearUnlockedUp & !GearDown & !GearLockedDown -> choice: ((moveup'=movedup));
			!RetGearVOrder & GearUnlockedUp & GearDown & !GearLockedDown -> choice: ((moveup'=gearunlockdown));
			!RetGearVOrder & GearUnlockedUp & GearDown & GearLockedDown -> choice: ((moveup'=0));
			
			RetGearVOrder & !(moveup = movingup) -> choice: ((moveup' = moveup+1));
			RetGearVOrder & moveup = movingup -> choice: ((moveup'=moveup));
			
			formula GearUnlockedDown := moveup >= gearunlockdown;
			formula GearUp := moveup >= movedup;
			formula GearLockedUp := moveup = movingup;
		endcomponent
		
		// A gear was unlocked there for the cylinder is now intermediate
		//formula GearCylinderInter := (GearExtending.GearUnlockedUp | GearRetracting.GearUnlockedDown) & !GearExtending.GearLockedDown & !GearRetracting.GearLockedUp;
		
		// The gear gets locked in high position 
		formula GearCylinderHigh := GearRetracting.GearLockedUp & !GearExtending.GearLockedDown;
		
		// The gear gets locked in down position
		formula GearCylinderDown := !GearRetracting.GearLockedUp & GearExtending.GearLockedDown;

	endcomponent
			
	component CylinderDoor
	
		// Works just like gear part.	
		component DoorOpening
			constant int moveddown:=doorunlockup+doordown;
			constant int movingdown:=doorunlockup+doordown+doorlockdown;
			movedown: [0 .. movingdown] init movingdown;
			
			!OpenDoorVOrder & !DoorUnlockedDown & !DoorUp & !DoorLockedUp -> choice: ((movedown'=movingdown));
			!OpenDoorVOrder & DoorUnlockedDown & !DoorUp & !DoorLockedUp -> choice: ((movedown'=moveddown));
			!OpenDoorVOrder & DoorUnlockedDown & DoorUp & !DoorLockedUp -> choice: ((movedown'=doorunlockup));
			!OpenDoorVOrder & DoorUnlockedDown & DoorUp & DoorLockedUp -> choice: ((movedown'=0));
			
			OpenDoorVOrder & !(movedown = movingdown) -> choice: ((movedown' = movedown+1));
			OpenDoorVOrder & movedown = movingdown -> choice: ((movedown'=movedown));
			
			formula DoorUnlockedUp := movedown >= doorunlockup;
			formula DoorDown := movedown >= moveddown;
			formula DoorLockedDown := movedown = movingdown;
		endcomponent
		
		component DoorClosing
			constant int movedup:=doorup;
			constant int movingup:=doorup+doorlockup;
			moveup: [0 .. movingup] init 0;
			
			!CloseDoorVOrder & !DoorUnlockedUp & !DoorDown & !DoorLockedDown -> choice: ((moveup'=movingup));
			!CloseDoorVOrder & DoorUnlockedUp & !DoorDown & !DoorLockedDown -> choice: ((moveup'=movedup));
			!CloseDoorVOrder & DoorUnlockedUp & DoorDown & !DoorLockedDown -> choice: ((moveup'=1));
			!CloseDoorVOrder & DoorUnlockedUp & DoorDown & DoorLockedDown -> choice: ((moveup'=0));
			
			CloseDoorVOrder & !(moveup = movingup) -> choice: ((moveup' = moveup+1));
			CloseDoorVOrder & moveup = movingup -> choice: ((moveup'=moveup));
			
			formula DoorUnlockedDown := moveup >= 1;
			formula DoorUp := moveup >= movedup;
			formula DoorLockedUp := moveup = movingup;
		endcomponent
		
		// A door was unlocked there for the cylinder is now intermediate
		//formula DoorCylinderInter := (DoorOpening.DoorUnlockedUp | DoorClosing.DoorUnlockedDown) & !DoorOpening.DoorLockedDown & !DoorClosing.DoorLockedUp;
		
		// The door gets locked in high position 
		formula DoorCylinderHigh := DoorClosing.DoorLockedUp & !DoorOpening.DoorLockedDown;
		
		// The door gets locked in down position
		formula DoorCylinderDown := !DoorClosing.DoorLockedUp & DoorOpening.DoorLockedDown;
		
	endcomponent
endtemplate