#!/bin/bash


gcc -E -x c -P -traditional-cpp LandingGear.saml > lgd.saml
sed -n '/@End-Of-Templates/q;p' LandingGear.saml > templates.saml.procssing

while grep "CPPT_" lgd.saml > /dev/null;
do
        sed -i ':a;N;$!ba;s/CPPT_/\nCPPT_/g' lgd.saml
        echo -e "$(cat templates.saml.procssing)\n\n$(cat lgd.saml)" > tmp.saml.processing
        gcc -E -x c -P -traditional-cpp tmp.saml.processing > lgd.saml
        rm tmp.saml.processing
done

rm templates.saml.procssing
sed -i ':a;N;$!ba;s/\; /;\n/g' lgd.saml
